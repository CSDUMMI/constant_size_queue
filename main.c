#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct {
    unsigned int head;
    unsigned int element_size;
    unsigned int size;
    void* elements;
} Queue;

typedef unsigned int uint;

Queue* init_queue(uint size, uint element_size) {
    Queue* q = (Queue*)malloc(sizeof(Queue));
    q->head = 0;
    q->element_size = element_size;
    q->size = size;
    q->elements = malloc(size * element_size);
    return q;
}

void* queue_push(void* value_pointer, Queue* q) {
    void* old_value = malloc(q->element_size);
    memcpy(old_value,(void*)(((char*)q->elements) + q->element_size*q->head), q->element_size);
    memcpy((void*)(((char*)q->elements) + q->element_size*q->head), value_pointer, q->element_size);
    q->head++;

    if(q->head > q->size)
    {
        q->head = 0;
    }
    return old_value;
}

void* queue_head(Queue* q) {
    return &(((char*)(q->elements))[q->head * q->element_size]);
}

void* queue_element(uint index, Queue* q) {
    void* element = &(((char*)(q->elements))[index * q->element_size]);
    return element;
}
struct Student {
    char* name;
    char suid[8];
    uint lessons;
};

int main(void) {
    Queue* q = init_queue(10, sizeof(int));
    for(int i = 0; i <= 10; i++) {
        printf("%d\n", *(int*)queue_push(&i,q));
    }

    for(int i = 10; i <= 20; i++) {
        printf("%d\n", *(int*)queue_push(&i, q));
    }

    Queue* sq = init_queue(10, sizeof(struct Student));

    struct Student student;
    student.name = strdup("Adam");
    strcpy(student.suid, "ABCDEFG");
    student.lessons = 100;

    struct Student old_stud = *(struct Student*)queue_push(&student, sq);
    printf("%s", old_stud.name);

    printf("\n%s\n", ((struct Student*)queue_element(0, sq))->name);
    return 0;
}
